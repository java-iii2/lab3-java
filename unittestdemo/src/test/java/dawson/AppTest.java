package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void shouldEcho5(){
        assertEquals("test echo returns 5", 5, App.echo(5));
    }

     @Test
    public void shouldReturn6(){
        assertEquals("test oneMoreInt returns 6", 6, App.oneMoreInt(5));
    }
}
